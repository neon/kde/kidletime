Source: kidletime
Priority: optional
Section: libs
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 3.0~),
               debhelper (>= 11~),
               doxygen,
               extra-cmake-modules (>= 5.51.0~),
               graphviz,
               libqt5sql5-sqlite,
               libqt5waylandclient5-dev (>= 5.15.2~) [linux-any],
               libqt5x11extras5-dev (>= 5.8.0~),
               libwayland-dev (>= 1.9~) [linux-any],
               libx11-dev,
               libx11-xcb-dev,
               libxcb-sync-dev,
               libxcb1-dev,
               libxss-dev [!hurd-any],
               pkgconf,
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               plasma-wayland-protocols,
               qtbase5-dev (>= 5.8.0~),
               qtbase5-private-dev,
               qttools5-dev-tools (>= 5.4),
               qtwayland5-dev-tools (>= 5.15.0~) [linux-any],
               wayland-protocols
Standards-Version: 4.1.4
Homepage: https://projects.kde.org/kidletime
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kidletime
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kidletime.git

Package: libkf5idletime-dev
Section: libdevel
Architecture: any
Depends: libkf5idletime5 (= ${binary:Version}), qtbase5-dev (>= 5.8.0~)
Recommends: libkf5idletime-doc (= ${source:Version})
Breaks: libkf5idletime-doc (<< 5.61.90-0), libkf5kdelibs4support-dev (<< 5.51)
Replaces: libkf5idletime-doc (<< 5.61.90-0)
Description: development headers for the kidletime library
 This package contains development files for building software that uses
 libraries from the kidletime KDE framework.

Package: libkf5idletime-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: library to provide information about idle time (documentation)
 This library contains a class that allows applications to watch for
 user activity or inactivity. It is useful not only for finding out about
 the current idle time of the PC, but also for getting notified upon idle
 time events, such as custom timeouts, or user activity.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf5idletime5
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Breaks: baloo-kf5 (<< 5.51), libkf5idletime-dev (<< 5.53.0)
Replaces: libkf5idletime-dev (<< 5.53.0)
Recommends: kwayland-integration
Description: library to provide information about idle time
 This library contains a class that allows applications to watch for
 user activity or inactivity. It is useful not only for finding out about
 the current idle time of the PC, but also for getting notified upon idle
 time events, such as custom timeouts, or user activity.
